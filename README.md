Decks for the [Anki](https://apps.ankiweb.net) open source flashcard learning system.

All decks are created by me, but the content is drawn from elsewhere.
